from collections import OrderedDict
from collections import namedtuple

import aiohttp
import asyncio
import datetime
import discord
import re
from PIL import Image
from discord.ext import commands
from io import BytesIO

from cogs.utils import checks
from cogs.utils.paginator import ListPaginator
from element_zero.util import database, errors, logging

DESCRIPTION = (
    'Hello! I\'m a bot with the purpose to serve humans "global" emoji from my internal database. '
    'Feel free to add your own global emoji with `{prefix}add`.\n\n'
    'Please be nice and don\'t add anything offensive, otherwise my moderators may be forced to blacklist you. This '
    'rule also applies to adding random faces to common names.'
    '\n\n'
    '**Usage:**\n'
    'Just send emoji like you would any other Discord emoji, for example `:thonk:`. Semicolons are supported too.'
    '\n\n'
    '**Emoji Expiry:**\n'
    'To prevent emoji from stagnating, they are removed after a month if they haven\'t been used.'
)

OFFICIAL_SERVER_ID = 391335889848238092

WELCOME_CHANNEL_ID = 392001067958796289
REPORTS_CHANNEL_ID = 392001951631409162
BLACKLISTS_CHANNEL_ID = 392002106154024961
LOG_CHANNEL_ID = 392002156628017154

DEVELOPER_ROLE = 391336035717480449
MODERATOR_ROLE = 392003608725553152
ABUSE_ROLE = 392003576110776320
SUPPORT_ROLE = 392003643248738306


def _check_role(ctx, ids, member_id):
    guild = ctx.bot.get_guild(OFFICIAL_SERVER_ID)
    roles = [x for x in guild.roles if x.id in ids]
    if not roles:
        return False

    member = guild.get_member(member_id)
    if member is None:
        return False

    for role in member.roles:
        if role in roles:
            return True
    return False


def check_roles(*ids):
    def predicate(ctx):
        return _check_role(ctx, ids, ctx.author.id)

    return commands.check(predicate)


class EmojiConverter(commands.Converter):
    async def convert(self, ctx, argument: str):
        argument = argument.strip(' :;')
        cog = ctx.bot.get_cog('Emoji')
        return await cog.db.get_emoji(argument)


class LimitedDict(OrderedDict):
    def __init__(self, limit: int):
        super().__init__()
        self.limit = limit

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        if len(self.keys()) > self.limit:
            self.popitem(last=False)


class Emoji:
    def __init__(self, liara):
        self.liara = liara

        self.db = database.DatabaseInterface(self.liara)
        self.logger = logging.Logger(liara, self.db)

        self.expiry = lambda: datetime.datetime.utcnow() - datetime.timedelta(weeks=4)

        self.task = self.liara.loop.create_task(self.loop())

        self.re_emoji = re.compile(r':([a-zA-Z0-9_\-]{2,32}):')
        self.re_alt_emoji = re.compile(r';([a-zA-Z0-9_\-]{2,32});')
        self.re_code = re.compile(r'`{1,3}.+?`{1,3}', re.DOTALL)
        self.re_custom_emoji = re.compile(r'<a?:[a-zA-Z0-9_\-]{2,32}:[0-9]+>')
        self.re_martmists = re.compile(r'\\:[a-zA-Z0-9_\-]{2,32}:')

        self.re_steal_emoji = re.compile(r'^\\?<a?:([a-zA-Z0-9_\-]{2,32}):([0-9]+)>$')

        self.invokes = LimitedDict(500)

        self.http = aiohttp.ClientSession(headers={
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                          'Chrome/65.0.3325.181 Safari/537.36',
            'DNT': '1'
        }, loop=self.liara.loop)

    def __unload(self):
        self.task.cancel()
        self.http.close()

    async def loop(self):
        while True:
            await self.liara.wait_until_ready()

            target = self.expiry()
            emojis = await self.db.get_emojis_to_delete(target, 1)

            for emoji in emojis:
                msg = await self.logger.log_emoji_decay(emoji)
                try:
                    await self.db.delete_emoji(emoji['name'])
                except errors.EmojiException:
                    await msg.delete()

            await asyncio.sleep(600)

    # helpers

    async def log_to_discord(self, channel_id, embed: discord.Embed):
        embed.timestamp = datetime.datetime.utcnow()

        ch = self.liara.get_channel(channel_id)
        if ch is None:
            return
        try:
            await ch.send(embed=embed)
        except discord.HTTPException:
            pass

    # informational commands

    @commands.command()
    async def about(self, ctx):
        """Lists information about the bot."""
        await ctx.trigger_typing()

        guilds = await self.db.list_backend_guilds(shuffle=False)
        static_emojis = await self.db.enumerate_emojis(animated=False)
        static_emoji_cap = len(guilds) * self.db.static_emoji_cap
        animated_emojis = await self.db.enumerate_emojis(animated=True)
        animated_emoji_cap = len(guilds) * self.db.animated_emoji_cap
        owner_emojis = await self.db.get_user_slots_used(ctx.author.id)
        owner_emoji_cap = await self.db.get_user_slots_capacity(ctx.author.id)

        e = discord.Embed()
        e.title = 'Element Zero Info'
        e.description = DESCRIPTION.format(prefix=self.liara.command_prefix[0])
        e.add_field(name='Backend Servers', value='{} servers connected'.format(len(guilds)), inline=True)
        e.add_field(
            name='Emoji Slots (Static)', value=f'{static_emojis} used of a total of {static_emoji_cap}', inline=True
        )
        e.add_field(
            name='Emoji Slots (Animated)',
            value=f'{animated_emojis} used of a total of {animated_emoji_cap}',
            inline=True
        )
        e.add_field(
            name='User Emoji Cap', value='{} of {} emojis uploaded'.format(owner_emojis, owner_emoji_cap), inline=True
        )
        await ctx.send(embed=e)

    @commands.command()
    async def staff(self, ctx):
        """Lists the bot's staff."""
        e = discord.Embed(
            title='Element Zero Staff Team',
            description='Running a bot like me isn\'t easy, so we have a dedicated staff team working to get you the '
            'best experience you can get.'
        )
        creators = ', '.join(sorted([str(self.liara.get_user(int(x))) for x in self.liara.owners])) + '\u200b'
        e.add_field(name='Creators - They wrote the book', value=creators, inline=False)

        guild = self.liara.get_guild(OFFICIAL_SERVER_ID)

        moderator_role = [x for x in guild.roles if x.id == MODERATOR_ROLE][0]
        moderators = ', '.join(sorted([str(x) for x in guild.members if moderator_role in x.roles])) + '\u200b'
        e.add_field(name='Moderators - They keep the official server squeaky clean', value=moderators, inline=False)

        abuse_role = [x for x in guild.roles if x.id == ABUSE_ROLE][0]
        abuse = ', '.join(sorted([str(x) for x in guild.members if abuse_role in x.roles])) + '\u200b'
        e.add_field(name='Abuse - They keep our emojis clean and respond to reports', value=abuse, inline=False)

        support_role = [x for x in guild.roles if x.id == SUPPORT_ROLE][0]
        support = ', '.join(sorted([str(x) for x in guild.members if support_role in x.roles])) + '\u200b'
        e.add_field(name='Support - They know their way around and help users', value=support, inline=False)

        await ctx.send(embed=e)

    @commands.command()
    @commands.cooldown(1, 60, commands.BucketType.channel)
    async def support(self, ctx):
        """Directs you to the offical support server."""
        channel = self.liara.get_channel(WELCOME_CHANNEL_ID)
        invite = await channel.create_invite(
            max_age=600, max_uses=10, reason=f'Created for {ctx.author} ({ctx.author.id})'
        )
        try:
            await ctx.author.send(f'Official support server invite: {invite.url}')
            await ctx.send('\N{OK HAND SIGN}')
        except discord.HTTPException:
            await ctx.send('Unable to send invite in DMs, please allow DMs from server members.')

    # real commands

    @commands.command()
    async def add(self, ctx, name: str, emoji_url=None):
        """Adds an emoji.

        This can be done either by uploading the emoji or by linking the emoji with its URL.

        * emoji_url: used if no emoji was uploaded with the message
        """
        await ctx.trigger_typing()

        if not emoji_url and ctx.message.attachments:
            emoji_url = ctx.message.attachments[0].url
        if not emoji_url:
            return await self.liara.send_command_help(ctx)

        try:
            async with self.http.head(emoji_url) as resp:
                if resp.reason != 'OK':
                    return await ctx.send(f'URL error: Server returned error code {resp.status}.')
                if resp.headers.get('Content-Type') not in ['image/png', 'image/jpeg', 'image/gif']:
                    return await ctx.send('URL specified is not a PNG, JPG or GIF.')
                animated = False
                if resp.headers['Content-Type'] == 'image/gif':
                    animated = True

            async with self.http.get(emoji_url) as resp:
                if resp.reason != 'OK':
                    return await ctx.send(f'URL error: Server returned error code {resp.status}.')

                buffer = BytesIO()
                while True:
                    if buffer.tell() > 1024**2 * 8:
                        return await ctx.send('Image may not be larger than 8 MiB.')

                    data = await resp.content.read(1024 * 1024)  # 1 MiB max chunk size
                    if not data:
                        break
                    buffer.write(data)
        except (ValueError, aiohttp.ClientConnectorError):
            return await ctx.send('URL error: URL given is invalid.')

        if not animated:
            out = BytesIO()

            i = Image.open(buffer)
            i.thumbnail((128, 128))
            i.save(out, 'png')
        else:
            out = buffer

        out.seek(0)

        try:
            emoji = await self.db.create_emoji(name, ctx.author.id, animated, out.read())
        except (errors.UserError, discord.HTTPException) as e:
            return await ctx.send(
                f'An error occurred while creating the emoji: {e}\n'
                f'For more information, please contact my support server.'
            )

        await self.logger.log_emoji_add(emoji)
        emoji = self.db.to_discord_emoji(emoji)
        await ctx.send(f'Emoji {emoji} successfully created.')

    @commands.command(aliases=['delete'])
    async def remove(self, ctx, name: str):
        """Removes an emoji owned by you.

        - name: the name of the emoji to delete
        """
        await ctx.trigger_typing()

        try:
            e = await self.db.delete_emoji(name, verify_owner=ctx.author.id)
        except errors.UserError as e:
            return await ctx.send(f'An error occurred while removing the emoji: {e}')
        except errors.DiscordInterfaceError:
            return await ctx.send('Discord seems to be having issues right now, try again later.')
        await self.logger.log_emoji_remove(e)
        await ctx.send('Emoji removed.')

    @commands.command()
    async def steal(self, ctx, emoji: str, new_name=None):
        """Steals an existing Discord emoji to the database.

        * new_name: the new name for the emoji, defaults to the Discord emoji name otherwise

        Arguments marked with * are optional.
        """
        match = self.re_steal_emoji.fullmatch(emoji)

        if match is None:
            raise commands.BadArgument('No emoji specified.')

        name = new_name if new_name else match.group(1)
        _format = 'png' if emoji[1] != 'a' else 'gif'
        url = f'https://cdn.discordapp.com/emojis/{match.group(2)}.{_format}'
        await ctx.invoke(self.add, name=name, emoji_url=url)

    # noinspection PyUnresolvedReferences
    @commands.command()
    async def react(self, ctx, emoji: EmojiConverter, *, keyword_or_id: str = None):
        """
        Reacts to a message with an emoji.

        - emoji: The emoji to react with.
        * keyword_or_id: The word to look for in the messages to react to. The first one will be chosen.
                         Can also be an ID.

        Arguments marked with * are optional.
        """
        if emoji is None:
            return await ctx.send('Emoji not found.')

        sender_permissions = ctx.channel.permissions_for(ctx.author)
        permissions = ctx.channel.permissions_for(ctx.guild.me)
        if not sender_permissions.read_message_history or not permissions.read_message_history:
            return await ctx.send('Unable to react, no permission to read message history.')
        if not sender_permissions.add_reactions or not permissions.add_reactions:
            return await ctx.send('Unable to react, no permission to add reactions.')

        # ID conversion if possible
        if keyword_or_id is not None and len(keyword_or_id) >= 17:  # based on lowest observed snowflake (Stan)
            try:
                keyword_or_id = int(keyword_or_id)
            except ValueError:
                pass

        found = None

        # lookup for ID / keyword
        if isinstance(keyword_or_id, int):
            try:
                found = await ctx.channel.get_message(keyword_or_id)
            except discord.HTTPException:
                pass
        else:
            async for msg in ctx.history():
                if ctx.message.content == msg.content:
                    continue
                if not keyword_or_id or keyword_or_id.lower() in msg.content.lower():
                    found = msg
                    break

        if found is None:
            return await ctx.send('No message with that keyword or ID found to react to.')

        await found.add_reaction(self.liara.get_emoji(emoji['id']))
        await ctx.send('Please react to the message with the emoji now. '
                       'It will be removed automatically in 10 seconds.',
                       delete_after=5)
        await asyncio.sleep(10)
        try:
            await found.remove_reaction(self.liara.get_emoji(emoji['id']), ctx.guild.me)
        except discord.HTTPException:
            pass  # /shrug, but it's gonna happen

    @commands.command()
    async def info(self, ctx, emoji: str):
        """Gives info on an emoji.

        - emoji: the emoji to get the info on
        """
        emoji = await self.db.get_emoji(emoji)
        if emoji is None:
            return await ctx.send('Emoji not found.')
        owner = self.liara.get_user(emoji['owner'])
        if owner is None:
            owner = 'Owner not found. User ID: {}'.format(emoji['owner'])
        else:
            owner = f'{owner.mention} ({owner})'
        verified = self.liara.get_user(emoji.get('verification', {}).get('moderator'))
        emoji = self.db.to_discord_emoji(emoji)

        embed = discord.Embed(title='Emoji Info')
        embed.add_field(name='Emoji', value=emoji)
        embed.add_field(name='Owner', value=owner)
        if verified:
            embed.add_field(name='Verified by', value=f'{verified.mention} ({verified})')
        await ctx.send(embed=embed)

    @commands.command()
    async def list(self, ctx):
        """Lists all emojis."""
        await ctx.send('The new emoji list can be found at <https://e0.qcx.io/list>.')

    @commands.command()
    async def popular(self, ctx, show_flagged: bool = False):
        """Lists popular emojis.

        * show_flagged: Whether or not to list flagged emojis.
        """
        emojis = await self.db.get_emojis()
        usage_counts = await self.db.get_emoji_usage()
        for emoji in emojis:
            _id = emoji['id']
            emoji['usage'] = usage_counts.get(_id, 0)
        del usage_counts  # save memory, the precious kilobytes aren't saving themselves

        if not show_flagged:
            emojis = [x for x in emojis if emojis if x.get('flag', {}).get('state', False) is False]

        emojis = emojis[0:200]  # first 200 only
        emojis = sorted(emojis, key=lambda x: x['usage'], reverse=True)

        processed = list()
        for emoji in emojis:
            discord_emoji = self.db.to_discord_emoji(emoji)

            owner_id = emoji['owner']
            owner = await self.db.find_user(owner_id)

            c = emoji['usage']
            multiple = '' if c == 1 else 's'

            processed.append(f'{discord_emoji}, used **{c}** time{multiple}, owned by **{owner}**')

        del emojis  # ditto

        paginator = ListPaginator(ctx, processed)
        await paginator.begin()

    @commands.command()
    @commands.guild_only()
    async def state(self, ctx, state: bool):
        """Switches behavior for you.

        - state: whether you want the bot to respond to your emojis
        """
        await self.db.set_user_state(ctx.author.id, state)
        if state:
            await ctx.send('Opted in to emojis.')
        else:
            await ctx.send('Opted out of emojis.')

    @commands.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_emojis=True)
    async def serverstate(self, ctx, state: bool):
        """Switches opt-in or opt-out mode.

        - state: whether users have to opt-in or opt-out for this bot on this server
        """
        await self.db.set_guild_state(ctx.guild.id, state)
        if state:
            await ctx.send('Opt-out mode enabled.')
        else:
            await ctx.send('Opt-in mode enabled.')

    # moderator commands

    @commands.command()
    @check_roles(DEVELOPER_ROLE, MODERATOR_ROLE, ABUSE_ROLE)
    async def purge(self, ctx, *names: str):
        """Forcibly removes emojis."""
        await ctx.trigger_typing()

        removed = []
        not_removed = []

        for i in names:
            try:
                await self.logger.log_emoji_purge(await self.db.get_emoji(i))
                await self.db.delete_emoji(i)
                removed.append(i)
            except errors.EmojiException:
                not_removed.append(i)

        if not_removed:
            await ctx.send('Not removed {}.'.format(', '.join(f'`{x}`' for x in not_removed)))
        if removed:
            await ctx.send('Removed {}.'.format(', '.join(f'`{x}`' for x in removed)))

    @commands.command()
    @check_roles(DEVELOPER_ROLE, MODERATOR_ROLE, ABUSE_ROLE)
    async def purgebyowner(self, ctx, user_id: int):
        """Forcibly removes all of a user's emojis."""

        def predicate(message):
            return message.author.id == ctx.author.id and message.channel.id == ctx.channel.id

        records = await self.db.get_emojis_by_owner(user_id)
        emojis = [x for x in records]
        await ctx.send(f'This will affect {len(emojis)} emojis, are you sure?')
        try:
            confirm = await self.liara.wait_for('message', timeout=60, check=predicate)
        except asyncio.TimeoutError:
            confirm = ctx.message  # this will never match, resulting in aborting the purge
        if confirm.content.lower() != 'yes':
            return await ctx.send('Purge aborted.')
        await ctx.send('Purging...')
        await ctx.trigger_typing()
        for emoji in emojis:
            try:
                await self.logger.log_emoji_purge(emoji)
                await self.db.delete_emoji(emoji['name'])
            except errors.EmojiNotFound:
                pass  # race conditions
            except errors.DiscordInterfaceError:
                return await ctx.send('Discord seems to be having issues, please try again later.')
        await ctx.send('Emojis deleted.')

    @commands.command()
    @check_roles(DEVELOPER_ROLE, MODERATOR_ROLE, ABUSE_ROLE)
    async def blacklist(self, ctx, user_id: int, *, reason: str = None):
        """Blacklists someone."""
        if reason is None:
            await self.db.set_user_blacklist(user_id)
        else:
            await self.db.set_user_blacklist(user_id, reason=reason, moderator=ctx.author.id)
        embed = discord.Embed()
        user = await self.db.find_user(user_id)
        embed.description = f'User affected: {user}'
        embed.set_footer(text=await self.db.find_user(ctx.author.id))
        if reason:
            embed.colour = discord.Colour.red()
            embed.title = 'Blacklist addition'
            embed.description += f'\nWith reason: `{reason}`'
        else:
            embed.colour = discord.Colour.green()
            embed.title = 'Blacklist removal'

        channel = self.liara.get_channel(BLACKLISTS_CHANNEL_ID)
        await channel.send(embed=embed)

        if reason is None:
            return await ctx.send('User unblacklisted.')
        await ctx.send(f'User blacklisted with reason `{reason}`.')

    @commands.command()
    @check_roles(DEVELOPER_ROLE, MODERATOR_ROLE)
    async def verify(self, ctx, *names):
        """Verifies emojis."""
        names = set(names)
        if not names:
            return await self.liara.send_command_help(ctx)
        for name in names:
            await self.db.set_emoji_verification(name, ctx.author.id)
        await ctx.send('\N{OK HAND SIGN}')

    @commands.command()
    @check_roles(DEVELOPER_ROLE, MODERATOR_ROLE)
    async def unverify(self, ctx, *names):
        """Unverifies emojis."""
        names = set(names)
        if not names:
            return await self.liara.send_command_help(ctx)
        for name in names:
            await self.db.set_emoji_verification(name, None)
        await ctx.send('\N{OK HAND SIGN}')

    @commands.command()
    @check_roles(DEVELOPER_ROLE, MODERATOR_ROLE, ABUSE_ROLE)
    async def flag(self, ctx, state: bool, *names):
        """Flags emojis."""
        names = set(names)
        if not names:
            return await self.liara.send_command_help(ctx)
        for name in names:
            await self.db.set_emoji_flag(name, state, ctx.author.id)
        await ctx.send('\N{OK HAND SIGN}')

    # events

    async def on_message(self, message):
        if message.author.bot:
            return
        if message.guild:
            if not message.guild.me.permissions_in(message.channel).external_emojis:
                return
        if not message.content:
            return
        if self.db is None:
            return

        content = message.content
        content = self.re_code.sub('', content)
        content = self.re_custom_emoji.sub('', content)
        content = self.re_martmists.sub('', content)

        emojis = self.re_emoji.findall(content) + self.re_alt_emoji.findall(content)
        if not emojis:
            return

        fetched_emojis = []

        if isinstance(message.channel, discord.DMChannel):
            guild = None
        else:
            guild = message.guild.id

        allowed = await self.db.get_state(message.author.id, guild)
        if not allowed:
            return

        emoji_used = []
        for emoji in emojis:
            fetched = await self.db.get_emoji(emoji)
            if fetched is None:
                continue
            if fetched['id'] not in [x['id'] for x in emoji_used]:
                emoji_used.append(fetched)
            fetched_emojis.append(self.db.to_discord_emoji(fetched))

        if not fetched_emojis:
            return
        blacklisted = await self.db.get_user_blacklist(message.author.id)
        if blacklisted:
            prefix = self.liara.command_prefix[0]
            try:
                await message.author.send(
                    f'You have been blacklisted from using emojis with the reason '
                    f'`{blacklisted}`. To appeal this, please join the Element Zero support '
                    f'server with `{prefix}support`.'
                )
            except discord.HTTPException:  # if we can't DM them, whatevs
                pass
            return

        for emoji in emoji_used:
            self.liara.dispatch('eezo_emoji_use', emoji['name'].lower(), message.author)
            await self.db.log_emoji_use(emoji, message.author.id, guild)

        try:
            msg = await message.channel.send(' '.join(fetched_emojis))
            self.invokes[message.id] = msg
        except discord.HTTPException:
            pass

    async def on_raw_message_delete(self, data):
        msg = self.invokes.pop(data.message_id, None)
        if msg is None:
            return

        try:
            await msg.delete()
        except discord.HTTPException:
            pass

    async def on_raw_bulk_message_delete(self, data):
        for _id in data.message_ids:
            d = namedtuple('data', 'message_id')(_id)
            await self.on_raw_message_delete(d)


def setup(liara):
    liara.add_cog(Emoji(liara))
